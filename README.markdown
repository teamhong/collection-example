Momenton Collection Example
===========================

This example converts flat employee data into a hierarchical structure based on the employee and manager relationship from the source data..

**input**: flat employee data containing employee and manager relationship.
**output**: organisation hierarchy.

Assumption
----------
1. Employee id is unique and not nullable.
2. There will always be one and only one CEO, who doesn't have a manager.
3. CEO is the top level of the organisation hierarchy.
4. An exception would be thrown if multiple employees with no manager are found.
5. An exception would be thrown if a manager is not a valid employee.
6. The structure conversion logic is specific to this example.

How To Run
----------
**Run**
> gradlew run

**Run Tests**
> gradlew test
