package au.com.momenton.collectionexample.exception;

/**
 * A custom exception to be thrown when invalid employee data is found.
 * */
public class InvalidEmployeeDataException extends RuntimeException{
    public InvalidEmployeeDataException(String message){
        super(message);
    }
}
