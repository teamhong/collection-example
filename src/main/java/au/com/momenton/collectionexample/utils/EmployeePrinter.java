package au.com.momenton.collectionexample.utils;

import au.com.momenton.collectionexample.entity.Employee;

public class EmployeePrinter {

    public static final String INDENT_SYMBOL = "  ";

    public void print(Employee employee){
        printData(employee, "", true);
    }

    protected void printData(Employee employee, String prefix, boolean isRoot) {
        String indent = prefix + (isRoot ? "" : INDENT_SYMBOL);
        System.out.println(indent + employee.getName());
        if(employee.getDirectReports() != null){
            for (Employee directReport : employee.getDirectReports()) {
                printData(directReport, indent, false);
            }
        }
    }
}
