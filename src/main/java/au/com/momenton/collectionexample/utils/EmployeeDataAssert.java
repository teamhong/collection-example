package au.com.momenton.collectionexample.utils;

import au.com.momenton.collectionexample.entity.Employee;
import au.com.momenton.collectionexample.exception.InvalidEmployeeDataException;

public class EmployeeDataAssert {
    /**
     * Throws {@code InvalidEmployeeDataException} if {@code employee} is not null.
     *
     * @param employee - employee to validate.
     * */
    public static void shouldBeNull(Employee employee, String errorMessage){
        if(employee != null){
            throw new InvalidEmployeeDataException(errorMessage);
        }
    }

    public static void shouldNotBeNull(Employee employee, String errorMessage) {
        if(employee == null){
            throw new InvalidEmployeeDataException(errorMessage);
        }
    }
}
