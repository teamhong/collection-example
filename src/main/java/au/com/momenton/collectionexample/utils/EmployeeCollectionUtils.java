package au.com.momenton.collectionexample.utils;

import au.com.momenton.collectionexample.entity.Employee;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides utility methods to help processing {@code Employee} collections..
 * */
public abstract class EmployeeCollectionUtils {

    /**
     * This method convert a flat employee list to a tree structure.
     * Although the utility method can be made more generic, it is not part of this example.
     *
     * @param employeeList - flat employee list to be converted
     * */
    public static Employee flatListToTree(List<Employee> employeeList){

        Employee ceo = null;

        // convert employee list into map, so later, each one of them can be found easily.
        Map<Integer, Employee> employeeMap = new HashMap<>();
        for (Employee employee : employeeList) {
            employeeMap.put(employee.getId(), employee);

            if (employee.getManagerId() == null) {
                //find ceo (a.k.a tree root) and throw exception as soon as another tree root is found.
                //this validation logic can be removed/extracted to other places, i.e. caller or DB, when this function is changed to a more generic one.
                EmployeeDataAssert.shouldBeNull(ceo, "Multiple employee with no manager id found.");
                ceo = employee;
            }
        }

        for (Employee employee : employeeList) {
            //for every employees except CEO, find their manager and link them up.
           if(employee.getManagerId() != null){
                Employee manager = employeeMap.get(employee.getManagerId());
               //this validation logic can be removed/extracted to other places, i.e. caller or DB, when this function is changed to a more generic one.
                EmployeeDataAssert.shouldNotBeNull(manager, "Manager id " + employee.getManagerId() + " is invalid.");
                manager.getDirectReports().add(employee);
            }
        }

        return ceo;
    }
}
