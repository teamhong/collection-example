package au.com.momenton.collectionexample.repository;

import au.com.momenton.collectionexample.entity.Employee;

import java.util.LinkedList;
import java.util.List;

/**
 * Hardcoded data list as data repository.
 * */
public class EmployeeRepositoryImpl implements EmployeeRepository {
    @Override
    public List<Employee> getEmployees(){
        List<Employee> employeeList = new LinkedList<>();
        employeeList.add(new Employee("Alan", 100, 150));
        employeeList.add(new Employee("Martin", 200, 100));
        employeeList.add(new Employee("Jamie", 150, null));
        employeeList.add(new Employee("Alex", 275, 100));
        employeeList.add(new Employee("Steve", 400, 150));
        employeeList.add(new Employee("David", 190, 400));
        return employeeList;
    }
}
