package au.com.momenton.collectionexample.repository;

import au.com.momenton.collectionexample.entity.Employee;

import java.util.List;


/**
 * Employee Repository.
 * */
public interface EmployeeRepository {
    List<Employee> getEmployees();
}
