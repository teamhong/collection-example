package au.com.momenton.collectionexample;

import au.com.momenton.collectionexample.entity.Employee;
import au.com.momenton.collectionexample.repository.EmployeeRepository;
import au.com.momenton.collectionexample.repository.EmployeeRepositoryImpl;
import au.com.momenton.collectionexample.utils.EmployeeCollectionUtils;
import au.com.momenton.collectionexample.utils.EmployeePrinter;

import java.util.List;

public class CollectionExampleRunner {

    private EmployeeRepository employeeRepository;
    private EmployeePrinter employeePrinter;

    public CollectionExampleRunner(EmployeeRepository employeeRepository, EmployeePrinter employeePrinter){
        super();
        this.employeeRepository = employeeRepository;
        this.employeePrinter = employeePrinter;
    }

    public void run(){
        //get input data
        List<Employee> employeeList = this.employeeRepository.getEmployees();
        //convert into hierarchical structure
        Employee ceo = EmployeeCollectionUtils.flatListToTree(employeeList);
        //print out data
        this.employeePrinter.print(ceo);
    }

    public static void main(String[] args){
        CollectionExampleRunner exampleRunner = new CollectionExampleRunner(new EmployeeRepositoryImpl(), new EmployeePrinter());
        exampleRunner.run();
    }
}
