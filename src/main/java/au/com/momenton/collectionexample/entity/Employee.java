package au.com.momenton.collectionexample.entity;

import java.util.LinkedList;
import java.util.List;

public class Employee {
    private String name;
    private Integer id;
    private Integer managerId;

    private transient List<Employee> directReports = new LinkedList<>();

    public Employee(){
        super();
    }

    public Employee(String name, Integer id, Integer managerId){
        this();
        this.name = name;
        this.id = id;
        this.managerId = managerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public List<Employee> getDirectReports() {
        return directReports;
    }

    public void setDirectReports(List<Employee> directReports) {
        this.directReports = directReports;
    }
}
