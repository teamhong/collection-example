package au.com.momenton.collectionexample.utils;

import au.com.momenton.collectionexample.entity.Employee;
import org.easymock.Capture;
import org.easymock.CaptureType;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(EmployeePrinter.class)
public class EmployeePrinterTest {

    @After
    public void teardown() {
        //reset System.out after each test as it is global and some test cases mocked it
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
    }

    @Test
    public void print() throws Exception {
        //create a partial mock of EmployeePrinter
        EmployeePrinter testObject = createPartialMock(EmployeePrinter.class, "printData");

        Employee employee = new Employee();

        //test if printData is called
        testObject.printData(employee, "", true);
        expectLastCall();

        replayAll();

        testObject.print(employee);

        verifyAll();
    }

    @Test
    public void printData() throws Exception {
        EmployeePrinter testObject = new EmployeePrinter();

        PrintStream outMock = createMock(PrintStream.class);
        System.setOut(outMock);

        Capture<String> outputStringCapture = Capture.newInstance();
        outMock.println(EasyMock.capture(outputStringCapture));
        expectLastCall();

        replayAll();

        Employee employee = new Employee("Jamie", 150, null);
        testObject.printData(employee, "", true);

        verifyAll();

        assertEquals(employee.getName(), outputStringCapture.getValue());
    }

    @Test
    public void printData_whenEmployeeIsNotAtTopLevel() throws Exception {
        EmployeePrinter testObject = new EmployeePrinter();

        PrintStream outMock = createMock(PrintStream.class);
        System.setOut(outMock);

        Capture<String> outputStringCapture = Capture.newInstance();
        outMock.println(EasyMock.capture(outputStringCapture));
        expectLastCall();

        replayAll();

        Employee employee = new Employee("Jamie", 150, null);
        testObject.printData(employee, "", false);

        verifyAll();

        assertEquals(EmployeePrinter.INDENT_SYMBOL+employee.getName(), outputStringCapture.getValue());
    }

    @Test
    public void printData_whenEmployeeHasDirectReports() throws Exception {
        EmployeePrinter testObject = new EmployeePrinter();

        PrintStream outMock = createMock(PrintStream.class);
        System.setOut(outMock);

        Capture<String> outputStringCapture = Capture.newInstance(CaptureType.ALL);
        outMock.println(EasyMock.capture(outputStringCapture));
        expectLastCall().times(3);

        replayAll();

        Employee employee = new Employee("Jamie", 150, null);
        employee.getDirectReports().add(new Employee("Alan", 100, 150));
        employee.getDirectReports().add(new Employee("Steve", 400, 150));

        testObject.printData(employee, "", true);

        verifyAll();

        List<String> outputs = outputStringCapture.getValues();

        assertEquals(3, outputs.size());
        assertEquals("Jamie", outputs.get(0));
        assertEquals(EmployeePrinter.INDENT_SYMBOL + "Alan", outputs.get(1));
        assertEquals(EmployeePrinter.INDENT_SYMBOL + "Steve", outputs.get(2));
    }
}