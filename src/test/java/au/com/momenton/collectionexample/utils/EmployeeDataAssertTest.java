package au.com.momenton.collectionexample.utils;

import au.com.momenton.collectionexample.entity.Employee;
import au.com.momenton.collectionexample.exception.InvalidEmployeeDataException;
import org.junit.Test;

public class EmployeeDataAssertTest {

    @Test
    public void shouldBeNull_shouldDoNothingWhenObjectIsNull() throws Exception {
        EmployeeDataAssert.shouldBeNull(null, "test");
    }

    @Test(expected = InvalidEmployeeDataException.class)
    public void shouldBeNull_shouldThrowExceptionWhenObjectIsNotNull() throws Exception {
        EmployeeDataAssert.shouldBeNull(new Employee(), "test");
    }

    @Test
    public void shouldNotBeNull_shouldDoNothingWhenObjectIsNotNull() throws Exception {
        EmployeeDataAssert.shouldNotBeNull(new Employee(), "test");
    }

    @Test(expected = InvalidEmployeeDataException.class)
    public void shouldNotBeNull_shouldThrowExceptionWhenObjectIsNull() throws Exception {
        EmployeeDataAssert.shouldNotBeNull(null, "test");
    }
}