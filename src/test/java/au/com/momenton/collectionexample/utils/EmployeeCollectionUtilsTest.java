package au.com.momenton.collectionexample.utils;

import au.com.momenton.collectionexample.entity.Employee;
import au.com.momenton.collectionexample.exception.InvalidEmployeeDataException;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EmployeeCollectionUtilsTest {
    @Test
    public void flatListToTree() throws Exception {
        //prepare test data
        List<Employee> employeeList = new LinkedList<>();
        Employee alan = new Employee("Alan", 100, 150);
        employeeList.add(alan);
        Employee martin = new Employee("Martin", 200, 100);
        employeeList.add(martin);
        Employee ceo = new Employee("Jamie", 150, null);
        employeeList.add(ceo);
        Employee alex = new Employee("Alex", 275, 100);
        employeeList.add(alex);
        Employee steve = new Employee("Steve", 400, 150);
        employeeList.add(steve);
        Employee david = new Employee("David", 190, 400);
        employeeList.add(david);
        //test data preparation end

        Employee result = EmployeeCollectionUtils.flatListToTree(employeeList);

        //verify result
        assertEquals(ceo, result);
        assertEquals(2, result.getDirectReports().size());
        assertTrue(result.getDirectReports().contains(alan));
        assertTrue(result.getDirectReports().contains(steve));
        assertEquals(2, alan.getDirectReports().size());
        assertTrue(alan.getDirectReports().contains(martin));
        assertTrue(alan.getDirectReports().contains(alex));
        assertEquals(1, steve.getDirectReports().size());
        assertEquals(david, steve.getDirectReports().get(0));
    }

    @Test(expected = InvalidEmployeeDataException.class)
    public void flatListToTree_whenMultipleCEOsFound() throws Exception {
        //prepare test data
        List<Employee> employeeList = new LinkedList<>();
        employeeList.add(new Employee("Alan", 100, 150));
        employeeList.add(new Employee("Matin", 200, 100));
        employeeList.add(new Employee("Jamie", 150, null));
        employeeList.add(new Employee("Alex", 275, null));
        //test data preparation end

        Employee result = EmployeeCollectionUtils.flatListToTree(employeeList);
    }

    @Test(expected = InvalidEmployeeDataException.class)
    public void flatListToTree_whenManagerIsNotValid() throws Exception {
        //prepare test data
        List<Employee> employeeList = new LinkedList<>();
        employeeList.add(new Employee("Alan", 100, 150));
        employeeList.add(new Employee("Matin", 200, 100));
        employeeList.add(new Employee("Jamie", 150, null));
        employeeList.add(new Employee("Alex", 275, 300));
        //test data preparation end

        Employee result = EmployeeCollectionUtils.flatListToTree(employeeList);
    }
}