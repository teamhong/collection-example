package au.com.momenton.collectionexample;

import au.com.momenton.collectionexample.entity.Employee;
import au.com.momenton.collectionexample.repository.EmployeeRepository;
import au.com.momenton.collectionexample.utils.EmployeeCollectionUtils;
import au.com.momenton.collectionexample.utils.EmployeePrinter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;

import static org.easymock.EasyMock.expect;
import static org.powermock.api.easymock.PowerMock.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(EmployeeCollectionUtils.class)
public class CollectionExampleRunnerTest {
    @SuppressWarnings("unchecked")
    @Test
    public void run() throws Exception {
        //prepare test object and test data
        EmployeeRepository repositoryMock = createMock(EmployeeRepository.class);
        EmployeePrinter employeePrinterMock = createMock(EmployeePrinter.class);
        PowerMock.mockStatic(EmployeeCollectionUtils.class);

        CollectionExampleRunner testObject = new CollectionExampleRunner(repositoryMock, employeePrinterMock);

        List<Employee> employeeList = createMock(List.class);
        Employee employee = createMock(Employee.class);

        //setup expected behaviours
        expect(repositoryMock.getEmployees()).andReturn(employeeList);
        expect(EmployeeCollectionUtils.flatListToTree(employeeList)).andReturn(employee);
        employeePrinterMock.print(employee);
        expectLastCall();

        replayAll();

        //start test
        testObject.run();

        verifyAll();
    }

}