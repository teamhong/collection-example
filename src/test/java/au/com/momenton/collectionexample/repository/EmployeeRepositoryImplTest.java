package au.com.momenton.collectionexample.repository;

import au.com.momenton.collectionexample.entity.Employee;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class EmployeeRepositoryImplTest {

    private EmployeeRepositoryImpl testObject;

    @Before
    public void setUp(){
        testObject = new EmployeeRepositoryImpl();
    }

    @Test
    public void getEmployees() throws Exception {
        List<Employee> result = testObject.getEmployees();
        //simple test here as the method returns a static set of data
        assertEquals(6, result.size());
    }

}